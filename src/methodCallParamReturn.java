import java.util.Scanner;


public class methodCallParamReturn {
    public int CompareNum(int x, int y)
    {
        System.out.println(x + ", " + y);
        return Math.max(x, y);
    }

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        methodCallParamReturn z = new methodCallParamReturn();
        System.out.println("Enter first number");
        int i = scn.nextInt();
        System.out.println("Enter second number");
        int j = scn.nextInt();
        int result = z.CompareNum(i,j);
        System.out.println("Big number = " + result);
    }
}
