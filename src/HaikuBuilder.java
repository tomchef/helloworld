import java.util.stream.IntStream;

public class HaikuBuilder {
    public static void main(String[] args) {
        String [][] strings = {
                { "An", "Old", "Silent", "Pond..." },
                { "A", "frog", "jumps", "into", "the", "pond," },
                { "splash!", "Silence", "again." }
        };

        System.out.println();
        for (String[] line : strings) {
            IntStream.range(0, line.length).forEach(i -> {
                System.out.print(line[i]);
                if (i + 1 < line.length) {
                    System.out.print(" ");
                } else {
                    System.out.print("\n");
                }
            });
        }
    }

}
