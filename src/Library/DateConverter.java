package Library;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.Arrays;
import java.util.Locale;

public class DateConverter {

    static String[] months = {"January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"};

    String[] monthsCases = {"Jan", "february", "march", "apr", "May", "June ",
            "July", "august", "Sep", "oct", "November", "December"};

    public static void main(String[] args) {

     /*   DateTimeFormatter myFormatter = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .appendPattern("[MMMM] [MMM]")
                .toFormatter(Locale.ENGLISH);

for (String i : months) {
        TemporalAccessor accessor = myFormatter.parse(i);
        System.out.println(accessor.get(ChronoField.MONTH_OF_YEAR));
        }
      */


        for (String i : months) {
            System.out.printf(" ");
            switch (i) {

                case "January":
                case "jan":
                    System.out.printf("1");
                    break;

                case "February":
                case "feb":
                    System.out.printf("2");
                    break;

                case "March":
                case "mar":
                    System.out.printf("3");
                    break;

                case "April":
                case "apr":
                    System.out.printf("4");
                    break;

                case "May":
                    System.out.printf("5");
                    break;

                case "June":
                case "jun":
                    System.out.printf("6");
                    break;

                case "July":
                case "jul":
                    System.out.printf("7");
                    break;

                case "August":
                case "aug":
                    System.out.printf("8");
                    break;

                case "September":
                case "sep":
                case "sept":
                    System.out.printf("9");
                    break;

                case "October":
                case "oct":
                    System.out.printf("10");
                    break;

                case "November":
                case "nov":
                    System.out.printf("11");
                    break;

                case "December":
                case "dec":
                    System.out.printf("12");
                    break;
            }

        }

    }
}
