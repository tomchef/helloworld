package Library;

import java.io.BufferedReader;

import java.io.FileReader;


public class Main {

    private static final String[][] myBooks = {
            {"Ulysses", "Joyce", "Dublin"},
            {"Gravitys Rainbow", "Pynchon", "WW2"},
            {"2666", "Bolano", "Mexico City"}};

    private static final String sourcePath = "C:\\Users\\thomas.lehane\\OneDrive - Accenture\\Downloads\\books.csv";
    private static final String destPath = "C:\\Users\\thomas.lehane\\OneDrive - Accenture\\Downloads\\mybooks.csv";

    public static void main(String[] args) {
        createLibrary();
    }

    private static void createLibrary() {
        Librarian library = new Librarian(sourcePath, destPath);

        System.out.println("...");
        library.loadBooks();

        for (String[] bookString : myBooks) {
            boolean success = library.addBook(bookString);
            if (!success) {
                System.out.println("Error");
            }
        }

        library.writeBooks();

        System.out.println("Catalogue");
        library.printBooks();
    }
}
